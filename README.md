﻿| Class  | Adding to the end | Adding by index | Reading | Removing |
|--------|-------------------|-----------------|---------|----------|
| Single | 72 ms.            | 200 ms.         | 0 ms.   | 185 ms.  | 
| Vector | 7 ms.             | 20 ms.          | 0 ms.   | 184 ms.  |
| Factor | 0 ms.             | 0 ms.           | 0 ms.   | 186 ms.  |
