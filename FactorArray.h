﻿#pragma once
#include <algorithm>
#include <iostream>

#include "BasicArray.h"

using namespace std;

template <typename T>
class FactorArray : public BasicArray<T>
{
public:
    
    FactorArray(int Factor, int InitLength);
    FactorArray();
    ~FactorArray();
    
    void Insert(T Item, int Index) override;
    void Add(T Item) override;
    
    T Remove(int Index) override;
    void Delete();
    
    
    size_t Length() override;
    T Get(int Index) override;
    
private:

    T* Array = nullptr;
    size_t VisibleArrayLength = 0, ActualArrayLength = 0;
    int Factor;

    bool IsNeedResize();
    void AddElementsToArray(T Item, int Index);
};

template <typename T>
FactorArray<T>::FactorArray(int Factor, int InitLength)
{
    Array = new T[InitLength];
    ActualArrayLength = InitLength;
    this->Factor = Factor; 
}

template <typename T>
FactorArray<T>::FactorArray()
{
    Array = new T[10];
    ActualArrayLength = 10;
    this->Factor = 50; 
}

template <typename T>
FactorArray<T>::~FactorArray()
{
    Delete();
}


template <typename T>
void FactorArray<T>::Add(T Item)
{
    if (!IsNeedResize())
    {
        AddElementsToArray(Item, Length());
        return;
    }

    size_t NewLength = ActualArrayLength * Factor;
    T* NewArray = new T[NewLength];

    move(Array, Array + NewLength, NewArray);
    NewArray[NewLength] = Item;

    Delete();
    Array = NewArray;
    
    ActualArrayLength = NewLength;
    VisibleArrayLength++;
   
}

template <typename T>
bool FactorArray<T>::IsNeedResize()
{
    return Array == nullptr || VisibleArrayLength >= ActualArrayLength; 
}

template <typename T>
void FactorArray<T>::Insert(T Item, int Index)
{
    if (!IsNeedResize())
    {
        AddElementsToArray(Item, Index);
        return;
    }
    
    size_t NewLength = ActualArrayLength + ActualArrayLength * Factor / 100;
    T* NewArray = new T[NewLength];

    move(Array, Array + NewLength, NewArray);
    NewArray[Index] = Item;
    
    move(Array + Index, Array + VisibleArrayLength, NewArray + Index + 1);
    
    Delete();
    Array = NewArray;

    VisibleArrayLength++;
    ActualArrayLength = NewLength;
}

template <typename T>
T FactorArray<T>::Remove(int Index)
{
    T RemovedElement = Array[Index];
    
    size_t NewLength = VisibleArrayLength - 1;
    T* NewArray = new T[NewLength];


    move(Array, Array + Index, NewArray);
    move(Array + Index + 1, Array + VisibleArrayLength, NewArray + Index);
    
    Delete();
    Array = NewArray;
    VisibleArrayLength = NewLength;
    
    return RemovedElement;
}

template <typename T>
void FactorArray<T>::Delete()
{
    auto It = Array + 0;

    if (It == nullptr)
    {
        delete[] Array;
    }
    Array = nullptr;
}

template <typename T>
size_t FactorArray<T>::Length()
{
    return VisibleArrayLength;
}

template <typename T>
T FactorArray<T>::Get(int Index)
{
    return Array[Index]; 
}

template <typename T>
void FactorArray<T>::AddElementsToArray(T Item, int Index)
{
    Array[Index] = Item;
    VisibleArrayLength++;
}