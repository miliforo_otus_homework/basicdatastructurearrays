﻿#pragma once
#include <algorithm>
#include <iostream>

#include "BasicArray.h"

using namespace std;

template <typename T>
class SingleArray : public BasicArray<T>
{
public:

    ~SingleArray();
    
    void Insert(T Item, int Index) override;
    void Add(T Item) override;
    bool AddFirstElement(T Item);

    void Insert(T* Item, int Index);
    void Add(T* Item);
    bool AddFirstElement(T* Item);
    
    T Remove(int Index) override;
    T* Remove(size_t Index);
    void Delete();
    
    
    size_t Length() override;
    T Get(int Index) override;
    T* Get(size_t Index);
    
private:

    T* Array = nullptr;
    size_t ArrayLength = 0;
};


template <typename T>
SingleArray<T>::~SingleArray()
{
    Delete();
}


template <typename T>
void SingleArray<T>::Add(T Item)
{
    if (AddFirstElement(Item))
    {
        return;
    }

    size_t NewLength = Length() + 1;
    T* NewArray = new T[NewLength];

    move(Array, Array + NewLength, NewArray);
    NewArray[NewLength] = Item;

    Delete();
    Array = NewArray;
    ArrayLength = NewLength;
}

template <typename T>
bool SingleArray<T>::AddFirstElement(T Item)
{
    if (Array == nullptr)
    {
        Array = new T[0];
        Array[0] = Item;
        return true;
    }
    return false;
}

template <typename T>
void SingleArray<T>::Insert(T* Item, int Index)
{
    if (AddFirstElement(Item))
    {
        return;
    }
    
    size_t NewLength = ArrayLength + 1;
    T* NewArray = new T[NewLength];

    move(Array, Array + NewLength, NewArray);
    NewArray[Index] = Item;
    
    move(Array + Index, Array + ArrayLength, NewArray + Index + 1);
    
    Delete();
    Array = NewArray;

    ArrayLength++;
}

template <typename T>
void SingleArray<T>::Add(T* Item)
{
    if (AddFirstElement(Item))
    {
        return;
    }

    size_t NewLength = Length() + 1;
    T* NewArray = new T[NewLength];

    move(Array, Array + NewLength, NewArray);
    NewArray[NewLength] = Item;

    Delete();
    Array = NewArray;
    ArrayLength = NewLength;
}

template <typename T>
bool SingleArray<T>::AddFirstElement(T* Item)
{
    if (Array == nullptr)
    {
        Array = new T[0];
        Array[0] = Item;
        return true;
    }
    return false;
}

template <typename T>
void SingleArray<T>::Insert(T Item, int Index)
{
    if (AddFirstElement(Item))
    {
        return;
    }
    
    size_t NewLength = ArrayLength + 1;
    T* NewArray = new T[NewLength];

    move(Array, Array + NewLength, NewArray);
    NewArray[Index] = Item;
    
    move(Array + Index, Array + ArrayLength, NewArray + Index + 1);
    
    Delete();
    Array = NewArray;

    ArrayLength++;
}

template <typename T>
T SingleArray<T>::Remove(int Index)
{
    T RemovedElement = Array[Index];
    
    size_t NewLength = ArrayLength - 1;
    T* NewArray = new T[NewLength];


    move(Array, Array + Index, NewArray);
    move(Array + Index + 1, Array + ArrayLength, NewArray + Index);
    
    Delete();
    Array = NewArray;
    ArrayLength = NewLength;
    
    return RemovedElement;
}

template <typename T>
T* SingleArray<T>::Remove(size_t Index)
{
    T* RemovedElement = Array[Index];
    
    size_t NewLength = ArrayLength - 1;
    T* NewArray = new T[NewLength];


    move(Array, Array + Index, NewArray);
    move(Array + Index + 1, Array + ArrayLength, NewArray + Index);
    
    Delete();
    Array = NewArray;
    ArrayLength = NewLength;
    
    return RemovedElement;
}

template <typename T>
void SingleArray<T>::Delete()
{
    auto It = Array + 0;

    if (It == nullptr)
    {
        delete[] Array;
    }
    Array = nullptr;
}

template <typename T>
size_t SingleArray<T>::Length()
{
    return ArrayLength;
}

template <typename T>
T SingleArray<T>::Get(int Index)
{
    return Array[Index]; 
}

template <typename T>
T* SingleArray<T>::Get(size_t Index)
{
    return Array[Index]; 
}


