﻿#pragma once

#include <iostream>
#include <string>
#include <chrono>

#include "FactorArray.h"
#include "MatrixArray.h"
#include "SingleArray.h"
#include "VectorArray.h"

using namespace std;
#define time chrono::time_point<chrono::system_clock>

class TestHandler
{
public:

    TestHandler(int NumberOfOperations):NumberOfOperations(NumberOfOperations){};

    const int NumberOfOperations;
    chrono::time_point<chrono::system_clock> StartTime;


    template <class T>
    void TestArray(BasicArray<T>* ArrayRef);
    
    void PrintResult(string Text);
    time Now() { return chrono::system_clock::now();}

    bool Adding = true;
    bool Reading = true;
    bool Removing = true;
    bool Inseting = true;
    
};

template <typename T>
void TestHandler::TestArray(BasicArray<T> *ArrayRef)
{
    if(Adding)
    {
        StartTime = Now();
        for (int i = 0; i <= NumberOfOperations; i++)
        {
            ArrayRef->Add(i);
        }
        PrintResult("Adding to the end of the array");
    }
    
    if (Inseting)
    {
        StartTime = Now();
        for (int i = 0; i <= NumberOfOperations; i++)
        {
            ArrayRef->Insert(i, i);
        }
        PrintResult("Adding by index in the array");
    }
    
    if(Reading)
    {
        StartTime = Now();
        for (int i = 0; i <= NumberOfOperations; i++)
        {
            ArrayRef->Get(i);
        }
        PrintResult("Reading elements");
    }

    if (Removing)
    {
        StartTime = Now();
        for (int i = 0; i < NumberOfOperations; i++)
        {
            ArrayRef->Remove(0);
        }
        PrintResult("Removing elements");
    }
}

inline void TestHandler::PrintResult(string Text)
{
    cout << Text << " " << chrono::duration_cast<std::chrono::milliseconds> (Now() - StartTime).count() << " ms" << std::endl;
}
