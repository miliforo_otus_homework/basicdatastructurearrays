﻿#pragma once

#include <algorithm>
#include <iostream>

#include "BasicArray.h"

using namespace std;

template <typename T>
class VectorArray : public BasicArray<T>
{
public:
    
    VectorArray(int Vector);
    VectorArray();
    ~VectorArray();
    
    void Insert(T Item, int Index) override;
    void Add(T Item) override;
    
    T Remove(int Index) override;
    void Delete();
    
    
    size_t Length() override;
    T Get(int Index) override;
    
private:

    T* Array = nullptr;
    size_t VisibleArrayLength = 0, ActualArrayLength = 0;
    const int Vector = 10;

    bool IsNeedResize();
    void AddElementsToArray(T Item, int Index);
};


template <typename T>
VectorArray<T>::VectorArray(int Vector): Vector(Vector)
{
    {};
}

template <typename T>
VectorArray<T>::VectorArray()
{
    Array = new T[Vector];
    ActualArrayLength = Vector;
}

template <typename T>
VectorArray<T>::~VectorArray()
{
    Delete();
}


template <typename T>
void VectorArray<T>::Add(T Item)
{
    if (!IsNeedResize())
    {
        AddElementsToArray(Item, Length());
        return;
    }

    size_t NewLength = ActualArrayLength + Vector;
    T* NewArray = new T[NewLength];

    move(Array, Array + NewLength, NewArray);
    NewArray[NewLength] = Item;

    Delete();
    Array = NewArray;
    
    VisibleArrayLength++;
    ActualArrayLength = NewLength;
}

template <typename T>
bool VectorArray<T>::IsNeedResize()
{
    return Array == nullptr || VisibleArrayLength >= ActualArrayLength; 
}

template <typename T>
void VectorArray<T>::Insert(T Item, int Index)
{
    if (!IsNeedResize())
    {
        AddElementsToArray(Item, Index);
        return;
    }
    
    size_t NewLength = ActualArrayLength + Vector;
    T* NewArray = new T[NewLength];

    move(Array, Array + NewLength, NewArray);
    NewArray[Index] = Item;
    
    move(Array + Index, Array + VisibleArrayLength, NewArray + Index + 1);
    
    Delete();
    Array = NewArray;

    VisibleArrayLength++;
    ActualArrayLength = NewLength;
}

template <typename T>
T VectorArray<T>::Remove(int Index)
{
    T RemovedElement = Array[Index];
    
    size_t NewLength = VisibleArrayLength - 1;
    T* NewArray = new T[NewLength];


    move(Array, Array + Index, NewArray);
    move(Array + Index + 1, Array + VisibleArrayLength, NewArray + Index);
    
    Delete();
    Array = NewArray;
    VisibleArrayLength = NewLength;
    
    return RemovedElement;
}

template <typename T>
void VectorArray<T>::Delete()
{
    auto It = Array + 0;

    if (It == nullptr)
    {
        delete[] Array;
    }
    Array = nullptr;
}

template <typename T>
size_t VectorArray<T>::Length()
{
    return VisibleArrayLength;
}

template <typename T>
T VectorArray<T>::Get(int Index)
{
    return Array[Index]; 
}

template <typename T>
void VectorArray<T>::AddElementsToArray(T Item, int Index)
{
    Array[Index] = Item;
    VisibleArrayLength++;
}


