﻿#pragma once

template <typename T>
struct BasicArray
{
    virtual ~BasicArray() {};

    virtual void Add(T Item) = 0;
    virtual void Insert(T Item, int index) = 0;

    virtual T Remove(int Index) = 0;
    
    virtual size_t Length() = 0;
    virtual T Get(int Index) = 0;
};


